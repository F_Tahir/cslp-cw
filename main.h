// Includes all function prototypes and global variables in the main.c source file

// Global Variables with descriptions
extern unsigned int busCapacity; // capacity of minibus in terms of no. of passengers
extern unsigned int boardingTime; // passenger boarding/disembarkation duration (in seconds)
extern float requestRate; // average number of journey requests per hour
extern float pickupInterval; // average time between request and desired pick up time (in mins)
extern unsigned int maxDelay; // average time a user can wait beyond desired pickup time (in minutes)
extern unsigned int noBuses; // number of minibuses
extern unsigned int noStops; // number of bus stops
extern unsigned int *map; // road layout and distances (in minutes) between bus stops
extern float stopTime; // Simulation duration (in hours)


extern unsigned int *noBusesArr; // multiple pickupInterval values stored here (i.e. if its an experiment)
extern unsigned int *maxDelayArr; // multiple maxDelay values stored here (i.e. if its an experiment)
extern int **mapMatrix; // 2d array which stores the matrix for the map
extern int noMatrixRows; // stores number of rows the matrix has (equal to n)

extern int sizeOfNoBusesArr; // stores the size of pickupIntervalArr if experiment option chosen
extern int sizeOfMaxDelayArr; // stores the size of maxDelayArr if experiment option chosen

extern int noBusesExperBool; // set to 1 if pickupInterval is an experiment, 0 otherwise
extern int maxDelayExperBool; // set to 1 if maxDelay is an experiment, 0 otherwise
extern int mapBoolean; // set to 1 if a map matrix is present in input file, 0 otherwise


extern int variableParsingOk; // set to 0 if variable parsing shows some error, and 1 otherwise
                                            // used in freeMemory() method to efficiently free dynamic arrays
extern int fileOpen; // set to 1 if fopen() function has been called and file is open, set to 0 if fclose() is called


// Function uses are described in main.c
void freeMemory();