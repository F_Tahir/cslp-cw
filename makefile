CFLAGS=-Wall -g
SOURCE=main.c variableParser.c simulator.c states.c unitTests.c output.c
EXECUTABLE=simulator

# Typing "make" compiles and links all source files
all: simulator

simulator: $(SOURCE)
	gcc  $(CFLAGS) -O3 -o $(EXECUTABLE) $(SOURCE) -lm

# Typing "make clean" removes the executable created when user typed "make"
clean:
	rm -f simulator


# Typing "make rb" rebuilds the executable - removes existing executable and recompiles/relinks all source files
rb: clean all
