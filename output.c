/* This file houses all functions needed for outputting events and statistics related to the simulator
Each functions usage is described where appropriate */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "simulator.h"
#include "states.h"
#include "time.h"
#include "main.h"

// Global Variables
int averageWaitingTime;
double averageTripDeviation;
double missedRequestPercentage;
double tripEfficiency;
int averageTripDurationMins;
int averageTripDurationSecs;

/* Prints out formatted events where user requests can be accommodated. */
void printNextRequest(int eventTime, int departureTime, int scheduleTime, int dptStop, int dstStop) {
    char* eventTimeFormatted = formatSecondsToString(eventTime);
    char* dptTime = formatSecondsToString(departureTime);
    char* schdTime = formatSecondsToString(scheduleTime);

    printf("%s -> new request placed from stop %d to stop %d for departure at %s scheduled for %s\n",
        eventTimeFormatted, dptStop, dstStop, dptTime, schdTime);

    // formatSecondsToString() function uses strdup, so freeing is necessary to avoid memory leaks.
    // The same follows for all print functions followed by this one.
    free((char*) eventTimeFormatted);
    free((char*) dptTime);
    free((char*) schdTime);
}


/* Prints out formatted events where user request cannot be accommodated. */
void printNextRequestFail(int eventTime, int departureTime, int dptStop, int dstStop) {
    char* eventTimeFormatted = formatSecondsToString(eventTime);
    char* dptTime = formatSecondsToString(departureTime);

    printf("%s -> new request placed from stop %d to stop %d for departure at %s cannot be accommodated\n", eventTimeFormatted, dptStop, dstStop, dptTime);

    free((char*) eventTimeFormatted);
    free((char*) dptTime);
}


/* Prints out formatted events where a bus left a stop. */
void printBusLeaveStop (int eventTime, int busIndex, int busStop) {
    char* eventTimeFormatted = formatSecondsToString(eventTime);

    printf("%s -> minibus %d left stop %d\n", eventTimeFormatted, busIndex+1, busStop);

    free((char*) eventTimeFormatted);
}


/* Prints out formatted events where a bus arrived at a stop. */
void printBusArriveStop (int eventTime, int busIndex, int busStop) {
    char* eventTimeFormatted = formatSecondsToString(eventTime);

    printf("%s -> minibus %d arrived at stop %d\n", eventTimeFormatted, busIndex+1, busStop);

    free((char*) eventTimeFormatted);
}


/* Prints out formatted events where a bus occupancy changed. */
void printBusOccupancyChange (int eventTime, int busIndex, int busOccupancy) {
    char* eventTimeFormatted = formatSecondsToString(eventTime);

    printf("%s -> minibus %d occupancy became %d\n", eventTimeFormatted, busIndex+1, busOccupancy);

    free((char*) eventTimeFormatted);
}


/* Prints out formatted events where a passenger disembarked bus. */
void printBusDisembarkPassenger (int eventTime, int busIndex, int busStop) {
    char* eventTimeFormatted = formatSecondsToString(eventTime);

    printf("%s -> minibus %d disembarked passenger at stop %d\n", eventTimeFormatted, busIndex+1, busStop);

    free((char*) eventTimeFormatted);
}


/* Prints out formatted events where a passenger boarded bus. */
void printBusBoardPassenger (int eventTime, int busIndex, int busStop) {
    char* eventTimeFormatted = formatSecondsToString(eventTime);

    printf("%s -> minibus %d boarded passenger at stop %d\n", eventTimeFormatted, busIndex+1, busStop);

    free((char*) eventTimeFormatted);
}


/* All my statistic interpretations are explained in the report, as there are multiple ways the statistics
can be interpreted and calculated.
Note that totalRequest is incremented even if the users departure time is greater than the stop time. */
double calcMissedRequests() {
    if (totalRequests != 0) {
        return ((double) missedRequests)/((double) totalRequests);
    } else {
        return 0.0;
    }

}

/* Because my simulator only works in a taxi fashion (i.e. one request per bus), the passenger in the
bus need not wait for any other passenger in-between stops so the waiting time will always be 0 */
int calcAvgWaitingTime() {
    return 0;
}


/* My interpretation of the duration of a passenger on buses is to not include boarding times.
I divide sumOfTripDifference by 60.0 to convert into minutes*/
double calcAvgTripDeviation() {
    int sumOfTripDifference = 0;
    int totalAccommodatedUsers = 0;
    int i;

    for (i = 0; i < noUsers; i++) {
        if (users[i].userPickedUp == 1 && users[i].userDroppedOff == 1) {
            totalAccommodatedUsers += 1;
            sumOfTripDifference += users[i].actualDistBetweenStops - users[i].minDistBetweenStops;
        }
    }
    if (totalAccommodatedUsers == 0) {
        return 0.0;
    } else {
        return (((double) sumOfTripDifference)/60.0) /((double)totalAccommodatedUsers);
    }
}


/* My interpretation for trip duration is *ONLY* when a passenger is present in the bus. Hence,
I do not include the time taken for the bus to travel to the users departure stop in the trip duration,
if the bus is empty. */
void calcAvgTripDuration() {
    int i;
    int totalTripTime = 0;
    int totalTrips = 0;
    double averageTripDuration;
    for (i = 0; i < noBuses; i++) {
        totalTripTime += buses[i].tripDuration;
        totalTrips += buses[i].noTrips;
    }

    if (totalTrips == 0) {
        averageTripDuration = 0.0;
    } else {
        averageTripDuration = round(totalTripTime/(double) totalTrips);
    }
    averageTripDurationMins = averageTripDuration/60;
    averageTripDuration -= averageTripDurationMins*60;
    averageTripDurationSecs = averageTripDuration;
}

/* I have discussed in my report the reasoning used for always returning 1.00 (bearing in mind
my simulator is taxi-style). Refer to 3.5.2 in report */
double calcTripEfficiency() {
    return 1.00;
}


void outputStatistics(int experimentNo, int maxDelayVal, int noBusesVal) {

    missedRequestPercentage = calcMissedRequests();
    averageWaitingTime = calcAvgWaitingTime();
    averageTripDeviation = calcAvgTripDeviation();
    calcAvgTripDuration();
    tripEfficiency = calcTripEfficiency();

    // Only print the experiment number and parameters if the input file contained the "experiment" keyword
    if (maxDelayExperBool == 1 || noBusesExperBool == 1) {
        printf("Experiment #%d:\tmaxDelay %d noBuses %d\n", experimentNo, maxDelayVal, noBusesVal);
    }

    printf("---\n");
    printf("average trip duration %02d:%02d\n", averageTripDurationMins, averageTripDurationSecs);
    printf("trip efficiency %.2f\n", tripEfficiency);
    printf("percentage of missed requests %.2f\n", missedRequestPercentage);
    printf("average passenger waiting time %d seconds\n", averageWaitingTime);
    printf("average trip deviation %.2f\n", averageTripDeviation);
    printf("---\n");
}