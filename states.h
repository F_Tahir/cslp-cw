// Includes all function prototypes and global variables in the states.c source file


// Stores the current state of buses (current position, capacity etc)
typedef struct {
    int currentBusCapacity;
    int currentRequests;   // stores the number of requests assigned to this bus - different from currentBusCapacity
                                    // takes into account requests where the passenger has not yet been picked up
                                    // aswell as passengers currently in the bus.
    int currentPosition;
    int isAtStop; // 1 if bus just left a stop and has not yet arrived at another
    int isIdle; // 1 if bus is idle at stop with no requests
    int noTrips;
    int tripDuration; // I do not take into account boarding and disembarking times for the journey
    int *departureStops; // stores the departure stop of each of the passengers assigned to bus
    int *destinationStops; // stores the destination stop of each of the passengers assigned to bus
    int *scheduledPickupTime; // stores the scheduled pick up time of each of the passengers assigned to bus
    int *scheduledDropoffTime; // stores the time the bus arrives at destination stop, disregarding boarding
    int *userID; // stores the index of all users who are assigned to this bus
} Bus;


// Stores information for each user, whether they were accommodated or not.
typedef struct {
    int requestTime;
    int departureTime;
    int scheduledTime; // -1 if user cannot be accommodated
    int departureStop;
    int destinationStop;
    int assignedBus; // busNumber count starts from 0
    int userPickedUp; // 1 if user has been picked up from departure stop, 0 otherwise
    int userDroppedOff; // 1 if user has been dropped off at destination stop, 0 otherwise
    int waitingTime; // used in "Average Waiting Time" statistic
    int minDistBetweenStops; /* same value as dist[departureStop][destinationStop], where dist is
                                            minimum distance matrix returned from Floyd-Warshall algorithm*/
    int actualDistBetweenStops; // Records the actual distance user has travelled since boarding
} User;


// Global Variables
extern Bus *buses;
extern User *users;


// Function uses are described in states.c
void setInitialBusState();
void setSizeOfUserStruct();
