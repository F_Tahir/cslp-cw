# This script will fail because stop 0 to stop 0 itself has a non 0 distance
# which is clearly an error as the distance from one stop to itself should always
# be 0. The error is:
# Error: Diagonal values of map matrix must be 0. Row 0, Column 0 (starting count from 0) is the culprit of this error. Please fix input file and re-run the simulator.


# capacity of a minibus in terms of number of passengers
busCapacity 12
# passenger boarding/disembarkation duration (in seconds)
boardingTime 15
# Average number of journey requests per hour
requestRate 30
# Average time between request and desired pick-up (in minutes)
pickupInterval 30
# Maximum time a user can wait beyond the desired pick-up time (in minutes)
maxDelay 25
# Number of minibuses
noBuses 5
# Number of bus stops
noStops 6
# Road layout and distances (in minutes) between bus stops
map
 1  3 -1 -1 -1  4
 3  0  5 -1 -1 -1
-1 -1  0  2 -1 -1
-1 -1 -1  0  2  2
-1  1 -1 -1  0 -1
 4 -1 -1  2  4  0
# Simulation duration (in hours)
stopTime 4