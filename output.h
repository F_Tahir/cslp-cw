// Includes all function prototypes and global variables in the output.c source file

// Global Variables
extern int averageWaitingTime;
extern double averageTripDeviation;
extern double missedRequestPercentage;
extern int tripEfficiency;
extern int averageTripDurationMins;
extern int averageTripDurationSecs;


// Function uses are described in output.c
void printNextRequest(int eventTime, int departureTime, int scheduleTime, int dptStop, int dstStop);
void printNextRequestFail(int eventTime, int departureTime, int dptStop, int dstStop);
void printBusLeaveStop (int eventTime, int busIndex, int busStop);
void printBusArriveStop (int eventTime, int busIndex, int busStop);
void printBusOccupancyChange (int eventTime, int busIndex, int busOccupancy);
void printBusDisembarkPassenger (int eventTime, int busIndex, int busStop);
void printBusBoardPassenger (int eventTime, int busIndex, int busStop);
void calcMissedRequests();
void calcAvgWaitingTime();
void calcAvgTripDeviation();
void calcAvgTripDuration();
void calcTripEfficiency();
void outputStatistics(int experimentNo, int maxDelayVal, int noBusesVal);