/* This file houses the main function as well as a freeMemory() function to free all memory
allocations done throughout the simulator.
Running valgrind (described in README) will show that no memory leaks are possible as I
have taken appropriate measures to ensure every memory allocation carried out
has been free'd after being used */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "variableParser.h"
#include "simulator.h"
#include "states.h"
#include "time.h"
#include "unitTests.h"
#include "output.h"


/* Throughout the code, global variables below are described in therespective header files,
and functions (where required) are described in the respective .c files */

// Global variables parsed from input file
/* Assign boardingTime/maxDelay to large numbers so I can check if values were
declared in input script. If == 20000000 after parsing, suggests values were not declared*/
unsigned int busCapacity = 0;
unsigned int boardingTime = 20000000;
float requestRate = 0.0;
float pickupInterval = 0.0;
unsigned int maxDelay = 20000000;
unsigned int noBuses = 0;
unsigned int noStops = 0;
int *map;
float stopTime = 0.0;

// Global variables used for matrix
unsigned int *noBusesArr;
unsigned int *maxDelayArr;
int **mapMatrix;
int noMatrixRows = 0;
int mapBoolean = 0;

// Global variables used for experimentation
int sizeOfNoBusesArr;
int sizeOfMaxDelayArr;
int noBusesExperBool = 0;
int maxDelayExperBool = 0;
int experimentCount = 0;

// Booleans used for freeMemory() method
int variableParsingOk = 0;
int fileOpen = 0;
int experimentationFinished = 0;


/* To avoid memory leaks, freeMemory() is called at the end of a simulation execution,
freeing all dynamically allocated arrays used during the simulation.
Using booleans to ensure we don't try to free memory that may not yet have been allocated*/
void freeMemory() {
        int y;

        if (fileOpen == 1)
            fclose(inputFile);
            fileOpen = 0;

        if (experimentationFinished == 1 || variableParsingOk == 0) {
            if (maxDelayExperBool == 1)
                free(maxDelayArr);

            if (noBusesExperBool == 1)
                free(noBusesArr);

            if (mapBoolean == 1)
                for (y = 0; y < noStops; y++)
                    free(mapMatrix[y]);
                free(mapMatrix);

            if (variableParsingOk == 1)
                for (y = 0; y < noStops; y++)
                    free(dist[y]);
                free(dist);

            if (variableParsingOk == 1)
                for (y = 0; y < noStops; y++)
                    free(nextHop[y]);
                free(nextHop);
        }

        // Free buses and user structs after every experiment because these structs
        // can change whereas the dynamically allocated memory above stays the same
        // throughout all experimentations
        if (variableParsingOk == 1)
            for (y = 0; y < noBuses; y++) {
                free(buses[y].departureStops);
                free(buses[y].destinationStops);
                free(buses[y].scheduledPickupTime);
                free(buses[y].scheduledDropoffTime);
                free(buses[y].userID);
            }
            free(buses);

        if (noUsers > 0)
            noUsers = 0;
            setSizeOfUserStruct(noUsers);
            free(users);
}


/* Main method links everything together - ensures input file is given, throws relevant errors if not.
Calls all relevant functions to start the simulation (or run tests if specified) */
int main(int argc, char *argv[]) {
    int i, j;
    srand ( (unsigned int) time ( NULL ) );
    if (argc <= 3 && argv[1] != NULL) {
        if (access(argv[1], F_OK) != -1) { // Checks whether given input file exists in directory
                if (argv[2] == NULL) {
                    extractVariables(argv[1]);
                    checkVariables();
                    calculateMinimumDist();

                    /* For experimentation support */
                    if (maxDelayExperBool == 1 && noBusesExperBool == 1) {
                        for (i = 0; i < sizeOfMaxDelayArr; i++) {
                            for (j = 0; j < sizeOfNoBusesArr; j++) {
                                maxDelay = maxDelayArr[i];
                                noBuses = noBusesArr[j];
                                setInitialBusState();
                                startSimulator();
                                experimentCount++;
                                outputStatistics(experimentCount, maxDelay, noBuses);
                                if (i*j==(sizeOfMaxDelayArr-1)*(sizeOfNoBusesArr-1))
                                    experimentationFinished = 1;
                                freeMemory();
                            }
                        }
                    } else if (maxDelayExperBool == 1 && noBusesExperBool == 0) {
                        for (i = 0; i < sizeOfMaxDelayArr; i++) {
                            maxDelay = maxDelayArr[i];
                            setInitialBusState();
                            startSimulator();
                            experimentCount++;
                            outputStatistics(experimentCount, maxDelay, noBuses);
                            if (i == sizeOfMaxDelayArr-1)
                                experimentationFinished = 1;
                            freeMemory();
                        }
                    } else if (maxDelayExperBool == 0 && noBusesExperBool == 1) {
                        for (i = 0; i < sizeOfNoBusesArr; i++) {
                            noBuses = noBusesArr[i];
                            setInitialBusState();
                            startSimulator();
                            experimentCount++;
                            outputStatistics(experimentCount, maxDelay, noBuses);
                            if (i == sizeOfNoBusesArr-1)
                                experimentationFinished = 1;
                            freeMemory();
                        }
                    } else if (maxDelayExperBool == 0 && noBusesExperBool == 0) {
                        setInitialBusState();
                        startSimulator();
                        outputStatistics(1, maxDelay, noBuses);
                        experimentationFinished = 1;
                        freeMemory();
                    }
                    /* Experimentation support ends and unit test support begins here*/
                } else if (strcmp(argv[2], "-runtests") == 0) { // Simulator will not run, unit tests will run instead
                    if (strcmp(argv[1], "Input Scripts/origInput.txt") == 0) {
                        extractVariables(argv[1]);
                        checkVariables();
                        calculateMinimumDist();
                        setInitialBusState();
                        runUnitTests();
                        experimentationFinished = 1;
                        freeMemory();
                    } else {
                        printf("Error: If you are using -runtests, please use the input file \"Input Scripts/origInput.txt\". "
                            "Consult the README if needed\n");
                    }
                } else {
                    printf("Error: Second parameter not recognized. Too many input files may have been given. "
                        "Consult the README file if needed.\n");
                }
        } else { // Given input file could not be accessed so second if statement (if (access...)) was not flagged
            printf("Error: Input file \"%s\" could not be opened. Ensure that the input file path has been typed properly. "
                "Consult the README file if needed.\n", argv[1]);
            exit(1);
        }

    } else { // Executed if no, or more than 2 arguments for input file has been given
        printf("Error: You have not given the simulator an input file, or too many parameters given. "
            "Consult the README file if needed.\n");
        exit(1);
    }
    return 0;
}
