#include <stdlib.h>
#include "main.h"
#include "states.h"
#include "simulator.h"

// Global Variables
Bus *buses;
User *users;

/* In this method, I create a (dynamic) array of structures to represent the state of the buses,
and set initial values (e.g. 0 for current position).*/
void setInitialBusState() {
    int i, j;
    buses = (Bus*) malloc(noBuses * sizeof(Bus)); // dynamically allocate the size of the array
    for (i = 0; i < noBuses; i++) {
        buses[i].departureStops = (int*) malloc(busCapacity * sizeof(int)); // dynamically allocate certain members of the Bus struct
        buses[i].destinationStops = (int*) malloc(busCapacity * sizeof(int));
        buses[i].scheduledPickupTime = (int*) malloc(busCapacity * sizeof(int));;
        buses[i].scheduledDropoffTime = (int*) malloc(busCapacity * sizeof(int));
        buses[i].userID = (int*) malloc(busCapacity * sizeof(int));
        buses[i].currentBusCapacity = 0;
        buses[i].currentRequests = 0;
        buses[i].currentPosition = 0;
        buses[i].isAtStop = 1;
        buses[i].isIdle = 1;
        buses[i].noTrips = 0;
        buses[i].tripDuration = 0;
        for (j = 0; j < busCapacity; j++) {
            buses[i].departureStops[j] = -1;
            buses[i].destinationStops[j] = -1;
            buses[i].scheduledPickupTime[j] = -1;
            buses[i].scheduledDropoffTime[j] = -1;
            buses[i].userID[j] = -1;
        }
    }
}


/* Each time noUsers is incremented, I pass the value to this method
to allocate more space for another user struct */
void setSizeOfUserStruct(int noUsers) {
    users = (User*) realloc (users, noUsers*sizeof(User));
}