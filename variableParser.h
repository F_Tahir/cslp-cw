// Includes all function prototypes and global variables in the variableParser.c source file


// Global variables
extern FILE *inputFile;


// Function uses are described in variableParser.c
int isNumeric (const char * s);
void extractVariables(char *inputFile);
void checkVariables();