/* This file houses the simulator algorithm and all functions needed to progress the simulator.
Each functions usage is described where appropriate */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "main.h"
#include "variableParser.h"
#include "states.h"
#include "time.h"
#include "output.h"


// Global variables and constants
#define NO_PATH 20000000
int **dist;
int **nextHop;
int sizeOfPath = 0;
int *path;
int noUsers = 0;
int userIndex;
int totalRequests = 0;
int missedRequests = 0;
int closestEventBusID;
int closestEventUserID;
int closestEventBusStop;
int indexOfBusArray;
int closestEventID;
/* getClosestEvent() assigns a number to closestEventID accordingly:
1 if the closest event is a new request which can or cannot be accommodated;
2 if the closest event is a bus arriving at a stop where a passenger was serviced;
3 if the closest event is a bus leaving a stop where a passenger was serviced;
4 if the closest event is a passenger boarding a bus;
5 if the closest event is a passenger disembarking a bus */


/* Uses pseudocode given for floyd-Warshall algorithm in slides
Since we don't know size of matrices before runtime (as noStops hasn't been parsed),
I use malloc to set the size of the matrices used in the Floyd-Warshall algorithm
Slight subtlety: dist[x][y] returns the smallest distance from stop x
to stop y, and does not necessarily have to be the smae as dist[y][x]
(which returns the smallest distance from y to x. This is because of the notion of one way roads*/
void calculateMinimumDist() {
    int i;
    int j;
    int k;
    int n = noStops;

    dist = malloc(sizeof(int*)*n);
        for (i = 0; i < n; i++)
            dist[i] = malloc(sizeof(int)*n);

    nextHop = malloc(sizeof(int*)*n);
        for (i = 0; i < n; i++)
            nextHop[i] = malloc(sizeof(int)*n);

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (mapMatrix[i][j] == -1) { // i.e. if no direct route exists between bus stop i and bus stop j
                dist[i][j] = NO_PATH; // NO_PATH is a constant defined to be a large number
            } else {
                dist[i][j] = mapMatrix[i][j];
            }
            nextHop[i][j] = j;
        }
    }

    for (k = 0; k < n; k++) {
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (dist[i][j] > dist[i][k] + dist[k][j]) {
                    dist[i][j] = dist[i][k] + dist[k][j];
                    nextHop[i][j] = nextHop[i][k];
                }
            }
        }
    }
}


/* This method returns the reconstructed shortest path between two bus stops,
i and j, using the nextHop matrix*/
int *reconstructPath(int i, int j) {
    sizeOfPath = 1;
    int a = 1;
    path = calloc(sizeOfPath, sizeof(int));
    path[0] = i;

    while (i != j) {
        i = nextHop[i][j];
        sizeOfPath += 1;
        path = realloc(path, sizeof(int)*sizeOfPath);
        path[a] = i;
        a++;
    }
    return path;
}


double uniform_deviate(int seed) {
    return rand() * (1.0 / (RAND_MAX + 1.0));;
}


/* In this method, the parameter rate is either pickupDelay or requestRate,
converted to work in second granularity, so that the output of this function
returns the time till either the next request, or the pickup delay, in seconds*/
int getEventTime(double rate) {
    double r;
    do {
        r = uniform_deviate( rand() );
    } while (r == 0);  // discard any 0 probabilities (log(0) is undefined)
    return round(-log(r)*(rate));
}


/*Generates a random bus stop number between 0 and noStops-1 for
user's desired departure stop*/
int getDepartureStop() {
    return rand() % (noStops);
}


/*Generates a random bus stop number between 0 and noStops-1 for user's desired
destination stop,ensuring the value generated differs from the departure stop
value, as user shouldn't depart from same stop as destination*/
int getDestinationStop(int departureStop) {
    int destinationStop;

    do {
        destinationStop = rand() % (noStops);
    } while (destinationStop == departureStop);
    return destinationStop;
}


/* Calculates the closest (NON-FULL) bus to some users' departure stop
Add 1 to the closestBus value when printing out bus numbers in event times.
This is because the bus count starts from 1 as opposed to 0, e.g. buses[0] = minibus 1
and buses[3] = minibus 4 etc..
Function returns -1 as the closest bus if all buses are full.*/
int getClosestBus(int departureStop) {
    int closestBus = -1, i;
    int distance = NO_PATH;

    for (i = 0; i < noBuses; i++) {
        if (buses[i].currentRequests < 1) {
            if (dist[buses[i].currentPosition][departureStop] <= distance) {
                closestBus = i;
                distance = dist[buses[i].currentPosition][departureStop];
            }
        }
    }
    return closestBus; // Add 1 to this value when printing bus number for events
}


/* Returns the distance from the closest bus (returned from getClosestBus() ) to
desired departure stop - used to check whether a request can be accommodated or not
due to distance being too far away
If all buses are full, returns NO_PATH (2000000) as distance*/
int getDistanceFromBus(int closestBus, int departureStop) {
    if (closestBus == -1) { // i.e. all buses full
        return NO_PATH;
    } else {
        return dist[buses[closestBus].currentPosition][departureStop];
        // Returns in minutes, need to convert to seconds
    }
}


/* Returns the index of the buses[busIndex].scheduledPickuptime array pointing to
the pick up request that occurs immediately before the new departure time. Returns -1 if no
prior pick up is in the system, and returns -10 if the new departure time overlaps a pickup
and dropoff time for the bus, meaning the bus cannot accommodate this new request. */
int getClosestPickupBefore(int busIndex, int departureTime) {
    int closestPickupBeforeVal = -1;
    int closestPickupBeforeIndex = -1;
    int i;

    for (i = 0; i < busCapacity; i++) {
        if (departureTime >= buses[busIndex].scheduledPickupTime[i] &&
            departureTime <= buses[busIndex].scheduledDropoffTime[i]) {
            closestPickupBeforeIndex = -10;
            break;
        } else if (buses[busIndex].scheduledPickupTime[i] > closestPickupBeforeVal &&
            buses[busIndex].scheduledPickupTime[i] <= departureTime) {

            closestPickupBeforeVal = buses[busIndex].scheduledPickupTime[i];
            closestPickupBeforeIndex = i;
        }
    }
    return closestPickupBeforeIndex;
}


/* Returns the index of the buses[busIndex].scheduledPickuptime array pointing to
the pick up request that occurs immediately after the new departure time. Returns -1 if no
prior pick up is in the system. */
int getClosestPickupAfter(int busIndex, int departureTime) {
    int closestPickupAfterVal = 3*stopTime*3600;
    int closestPickupAfterIndex = -1;
    int i;

    for (i = 0; i < busCapacity; i++) {
        if (buses[busIndex].scheduledPickupTime[i] < closestPickupAfterVal &&
                buses[busIndex].scheduledPickupTime[i] >= departureTime) {

                closestPickupAfterVal = buses[busIndex].scheduledPickupTime[i];
                closestPickupAfterIndex = i;
        }
    }
    return closestPickupAfterIndex;
}


// Formats seconds into <day:hour:minute:second> format to be used in the simulator output
char* formatSecondsToString(int seconds) {
    char formattedTime[13];
    int days = seconds/(24*3600);

    seconds -= days*24*3600;  //remainder in seconds after counting number of days
    int hours = seconds/3600;

    seconds -= hours*3600;   // remainder in seconds after counting number of hours
    int minutes = seconds/60;

    seconds -= minutes*60;  // emainder in seconds after counting number of minutes
    sprintf( formattedTime, "%02d:%02d:%02d:%02d", days, hours, minutes, seconds);

    return strdup(formattedTime);
}


/* This method returns the closest possible schedule time when a request is being handled.
If the request cannot be accommodated due to the departure time being too close
to the request time, and the departure stop being too far away (or if all buses are full), then schedule
time is assigned to be -1*/
int assignScheduleTime(int requestTime, int departureTime, int departureStop, int destinationStop) {
    int assignedBus = -1;
    int assignedSchdTime = -1;
    int assignedBusDist;
    int i, k = 0;
    int allBusesFull = 0;
    int closestPickupBeforeIndex;
    int closestPickupAfterIndex;


    for (i = 0; i < noBuses; i++) {
        if (buses[i].currentRequests >= 1) {
            allBusesFull = 1;
        } else {
            allBusesFull = 0;
            break;
        }
    }


    if (allBusesFull != 1) {
        assignedBus = getClosestBus(departureStop);
        assignedBusDist = getDistanceFromBus(assignedBus, departureStop);
        // There is some bus that can get there on time
        if (requestTime + (assignedBusDist*60) <= departureTime) { // can assign with no delay
            assignedSchdTime = departureTime;
        } else {
            // The closest bus is too far away, even taking the maxDelay into account, so the request
            // cannot be accommodated.
            if (departureTime + (maxDelay*60) <= requestTime + (assignedBusDist*60)) {
                assignedSchdTime = -1;
            // Can be assigned but some delay is needed
            } else {
                assignedSchdTime = requestTime + assignedBusDist*60;
            }
        }

    /* All buses have no space because either:
        - The bus is full with passengers, or
        - Some new request came for this bus that will make it full */
    } else if (allBusesFull == 1) {
         for (i = 0; i < noBuses; i++) {
            closestPickupBeforeIndex = getClosestPickupBefore(i, departureTime);
            closestPickupAfterIndex = getClosestPickupAfter(i, departureTime);
            // If the departure time overlaps a pickup and dropoff time for the given bus, or if
            // the bus has too many requests, check the next bus
            if (closestPickupBeforeIndex == -10 || buses[i].currentRequests >= busCapacity) {
                continue;
            } else {
                /* There exists some request that needs to be dealt with after this new request.*/
                if (closestPickupBeforeIndex == -1 && closestPickupAfterIndex != -1) {
                    if (departureTime + 2*boardingTime + dist[departureStop][destinationStop]*60 +
                        dist[destinationStop][buses[i].departureStops[closestPickupAfterIndex]]*60 <=
                        buses[i].scheduledPickupTime[closestPickupAfterIndex] && requestTime +
                        dist[buses[i].currentPosition][departureStop]*60 <= departureTime) {
                        assignedSchdTime = departureTime;
                        assignedBus = i;
                        break;
                    } else {
                        assignedSchdTime = -1;
                    }
                /* There exists some request that needs to be dealt with immediately after the new
                request.*/
                } else if (closestPickupBeforeIndex != -1 && closestPickupAfterIndex == -1) {
                    if (buses[i].scheduledDropoffTime[closestPickupBeforeIndex] + boardingTime +
                        dist[buses[i].destinationStops[closestPickupBeforeIndex]][departureStop]*60 <=
                        departureTime && requestTime + dist[departureStop][destinationStop]*60
                        <= departureTime) {

                        assignedSchdTime = departureTime;
                        assignedBus = i;
                        break;
                    } else {
                        assignedSchdTime = -1;
                    }
                /* There exists some request that needs to be dealt with immediately before and
                immediately after the new request.*/
                } else if (closestPickupBeforeIndex != -1 && closestPickupAfterIndex != -1) {
                    if (buses[i].scheduledDropoffTime[closestPickupBeforeIndex] + boardingTime +
                        dist[buses[i].destinationStops[closestPickupBeforeIndex]][departureStop]*60
                        <= departureTime && departureTime+boardingTime+dist[departureStop][destinationStop]*60
                        + boardingTime + dist[destinationStop][buses[i].departureStops[closestPickupAfterIndex]]*60 <=
                         buses[i].scheduledPickupTime[closestPickupAfterIndex]) {

                        assignedSchdTime = departureTime;
                        assignedBus = i;
                        break;
                    } else {
                        assignedSchdTime = -1;
                    }
                } else {
                    assignedSchdTime = -1;
                }
            }
        }
    }

    /* Set the state of the bus which will accommodate the user request */
    if (assignedSchdTime != -1) {
        while (buses[assignedBus].departureStops[k] != -1 && buses[assignedBus].departureStops[k] != -1) {
                k++;
            }
        buses[assignedBus].departureStops[k] = departureStop;
        buses[assignedBus].destinationStops[k] = destinationStop;
        buses[assignedBus].scheduledPickupTime[k] = assignedSchdTime;
        buses[assignedBus].scheduledDropoffTime[k] = assignedSchdTime + boardingTime + dist[departureStop][destinationStop]*60;
        buses[assignedBus].userID[k] = userIndex;
        buses[assignedBus].currentRequests += 1;
    }
    users[userIndex].assignedBus = assignedBus;

    return assignedSchdTime;
}


/* Returns the delay based on the nearest event, by looking at the current
states represented in the buses and user structs. ClosestEventID is set accordingly */
int getClosestEvent(int currTime) {
    int closestEvent = stopTime*3600;
    int i, j;
    int nextRequest = getEventTime(3600/requestRate);


    if (nextRequest + currTime <= stopTime*3600) {
        if (nextRequest <= closestEvent) {
            closestEvent = nextRequest;
            closestEventID = 1;
        }
    }


    /* Loop through the bus struct to obtain information on the current state. */
    for (i = 0; i < noBuses; i++) {
        int currPosition = buses[i].currentPosition;
        for (j = 0; j < busCapacity; j++) {
            if (buses[i].userID[j] != -1) {
                int schedTime = buses[i].scheduledPickupTime[j];
                int dropTime = buses[i].scheduledDropoffTime[j];
                int currUser = buses[i].userID[j];
                int userDptStop = users[currUser].departureStop;
                int userDstStop = users[currUser].destinationStop;

                // Checks to see if the next event is bus arriving at users departure stop
                if (users[currUser].userPickedUp == 0 && currPosition != userDptStop) {
                    if (schedTime - currTime <= closestEvent) {
                        closestEvent = schedTime - currTime; // time remaining till user gets picked up
                        closestEventID = 2;
                        closestEventBusID = i;
                        closestEventBusStop = userDptStop;
                        closestEventUserID = currUser;
                        indexOfBusArray = j;
                    }
                }
                // Checks to see if the next event is bus arriving at users destination stop
                if (users[currUser].userPickedUp == 1 && users[currUser].userDroppedOff == 0 &&
                    currPosition != userDstStop) {
                    if (dropTime - currTime <= closestEvent) {
                        closestEvent = dropTime - currTime;
                        closestEventID = 2;
                        closestEventBusID = i;
                        closestEventBusStop = userDstStop;
                        closestEventUserID = currUser;
                        indexOfBusArray = j;
                    }
                }
                // Checks to see if the next event is bus leaving idle stop to deal with a new request
                if (buses[i].isIdle == 1 && buses[i].isAtStop == 1 && users[currUser].departureStop != buses[i].currentPosition) {
                    if (schedTime - currTime - dist[currPosition][userDptStop]*60 <= closestEvent) {
                        closestEvent =  schedTime - currTime - dist[currPosition][userDptStop]*60;
                        closestEventID = 3;
                        closestEventBusID = i;
                        closestEventBusStop = currPosition;
                        indexOfBusArray = j;
                    }
                }
                // Checks to see if the next event is bus leaving users departure stop to go to destination stop
                if (buses[i].currentBusCapacity != 0 && currPosition == userDptStop && users[currUser].userPickedUp
                    == 1 && users[currUser].userDroppedOff == 0 && buses[i].isAtStop == 1) {
                    if (0 <= closestEvent) {
                        closestEvent = 0;
                        closestEventID = 3;
                        closestEventBusID = i;
                        closestEventBusStop = userDptStop;
                        indexOfBusArray = j;
                    }
                }
                // Checks to see if the next event is a user boarding a bus
                if (users[currUser].userPickedUp == 0 && buses[i].isAtStop == 1 && currPosition == users[currUser].departureStop) {
                    if (users[currUser].scheduledTime - currTime + boardingTime <= closestEvent) {
                        closestEvent = users[currUser].scheduledTime - currTime + boardingTime;
                        closestEventID = 4;
                        closestEventBusID = i;
                        closestEventBusStop = currPosition;
                        closestEventUserID = currUser;
                        indexOfBusArray = j;
                    }
                }
                // Checks to see if the next event is a user disembarking a bus
                if (users[currUser].userPickedUp == 1 && users[currUser].userDroppedOff == 0 && currPosition == userDstStop
                    && buses[i].isAtStop == 1 && buses[i].currentBusCapacity > 0) {
                    if ((dropTime + boardingTime) - currTime <= closestEvent) {
                        closestEvent = (dropTime + boardingTime) - currTime;
                        closestEventID = 5;
                        closestEventBusID = i;
                        closestEventBusStop = currPosition;
                        closestEventUserID = currUser;
                        indexOfBusArray = j;
                    }
                }
            }
        }
    }
    return closestEvent;
}

/* Essentially uses the algorithm given in the handout, calling getClosestEvent() to work out
the delay, incrementing the current time by the closest delay, and changing states depending
on what the closest event was */
void startSimulator() {
    int currTime = 0;
    int endTime = stopTime*3600;
    totalRequests = 0;
    missedRequests = 0;

    while (currTime <= endTime) {
        int closestEventDelay = getClosestEvent(currTime);

        currTime = currTime + closestEventDelay;

        if (currTime >= endTime) break;


        /* If the closest event is a new request, need to generate departure stop, destination stop,
        and generate a departure time based on the pickupInterval value.  */
        if (closestEventID == 1) {
            noUsers+=1;
            setSizeOfUserStruct(noUsers); // Allocate more memory for user struct
            userIndex = noUsers-1;

            // Current event is a request, so we have to generate components for this request
            int requestTime = currTime;
            int departureStop = getDepartureStop();
            int destinationStop = getDestinationStop(departureStop); // Ensure destination and departure is different
            int departureTime = requestTime + getEventTime(60*pickupInterval);
            int scheduleTime = assignScheduleTime(requestTime, departureTime, departureStop, destinationStop);
            totalRequests++;

            // Change the state based on current event
            users[userIndex].requestTime = requestTime;
            users[userIndex].departureTime = departureTime;
            users[userIndex].scheduledTime = scheduleTime; // -1 if the request could not be accommodated
            users[userIndex].departureStop = departureStop;
            users[userIndex].destinationStop = destinationStop;
            users[userIndex].userPickedUp = 0;
            users[userIndex].userDroppedOff = 0;
            users[userIndex].waitingTime = 0;
            users[userIndex].minDistBetweenStops = dist[departureStop][destinationStop]*60;
            users[userIndex].actualDistBetweenStops = 0;


            /* If the request can be accommodated, update states accordingly, and output event */
            if (scheduleTime != -1) {
                if (noBusesExperBool == 0 && maxDelayExperBool == 0) {
                    printNextRequest(currTime, users[userIndex].departureTime, users[userIndex].scheduledTime,
                        users[userIndex].departureStop, users[userIndex].destinationStop);
                }

            /* If the request cannot be accommodated, update states accordingly, and output event */
            } else if (scheduleTime == -1) {
                if (noBusesExperBool == 0 && maxDelayExperBool == 0) {
                    printNextRequestFail(currTime, users[userIndex].departureTime,
                        users[userIndex].departureStop, users[userIndex].destinationStop);
                }
                missedRequests++;
            }


        /* Closest event ID is the bus arriving at a stop where a user needs to be serviced.*/
        } else if (closestEventID == 2) {
            // Change states of corresponding bus that arrived at a stop
            buses[closestEventBusID].currentPosition = closestEventBusStop;
            buses[closestEventBusID].isAtStop = 1;

            if (noBusesExperBool == 0 && maxDelayExperBool == 0) {
                printBusArriveStop(currTime, closestEventBusID, closestEventBusStop);
            }


        /* Closest event ID is the bus leaving a stop where a user has just been serviced. */
        } else if (closestEventID == 3) {
            // Change states of corresponding bus that left a stop
            buses[closestEventBusID].isAtStop = 0;
            buses[closestEventBusID].isIdle = 0;

            if (noBusesExperBool == 0 && maxDelayExperBool == 0) {
                printBusLeaveStop(currTime, closestEventBusID, closestEventBusStop);
            }


        /* Closest event is passenger boarding bus.*/
        } else if (closestEventID == 4) {
            // Change states of corresponding bus that left a stop
            buses[closestEventBusID].currentBusCapacity +=1;

            // Change states of corresponding user being picked up
            users[closestEventUserID].userPickedUp = 1;

            if (noBusesExperBool == 0 && maxDelayExperBool == 0) {
                printBusBoardPassenger(currTime, closestEventBusID, closestEventBusStop);
                printBusOccupancyChange(currTime, closestEventBusID, buses[closestEventBusID].currentBusCapacity);
            }


        /* Closest event is passenger disembarking bus bus.*/
        } else if (closestEventID == 5) {
            // Change states of corresponding user being dropped off
            users[closestEventUserID].userDroppedOff = 1;
            users[closestEventUserID].userPickedUp = 1;
            users[closestEventUserID].actualDistBetweenStops =
                buses[closestEventBusID].scheduledDropoffTime[indexOfBusArray] -
                (buses[closestEventBusID].scheduledPickupTime[indexOfBusArray] + boardingTime);

            // Change states of corresponding bus that dropped off user
            buses[closestEventBusID].currentBusCapacity -= 1;
            buses[closestEventBusID].currentRequests -= 1;
            buses[closestEventBusID].noTrips += 1;
            buses[closestEventBusID].tripDuration += buses[closestEventBusID].scheduledDropoffTime[indexOfBusArray]
                - (buses[closestEventBusID].scheduledPickupTime[indexOfBusArray] + boardingTime);
            buses[closestEventBusID].scheduledPickupTime[indexOfBusArray] = -1;
            buses[closestEventBusID].scheduledDropoffTime[indexOfBusArray] = -1;
            buses[closestEventBusID].departureStops[indexOfBusArray] = -1;
            buses[closestEventBusID].destinationStops[indexOfBusArray] = -1;
            buses[closestEventBusID].userID[indexOfBusArray] = -1;
            if (buses[closestEventBusID].currentBusCapacity == 0)
                buses[closestEventBusID].isIdle = 1;

            if (noBusesExperBool == 0 && maxDelayExperBool == 0) {
                printBusDisembarkPassenger(currTime, closestEventBusID, closestEventBusStop);
                printBusOccupancyChange(currTime, closestEventBusID, buses[closestEventBusID].currentBusCapacity);
            }
        }

    } // Ends while loop
   /*int i, j;
    for (i = 0; i < noStops; i++) {
        for (j = 0; j < noStops; j++) {
            printf("dist[%d][%d] = %d\n", i, j, dist[i][j]);
        }
    }*/
}