// Includes all function prototypes and global variables in the unitTests.c source file


// Global variables with descriptions
extern int totalTests;
extern int failedTests;
extern int passedTests;



// Function uses are described in unitTests.c
void test_reconstructPath(int a, int b, int path[]);
void test_distMatrix(int a, int b, int distance);
void test_formatSecondsToString(char* s, int seconds);
void test_getDistanceFromBus(int bus, int stop, int distance);
void test_getClosestBus(int stop, int bus);
void test_departureStop();
void test_destinationStop(int departureStop);
void runUnitTests();