/* Unit tests for various functions used throughout the simulator. These tests can by run by typing :
"./simulator <inputfile> -runtests"
More information given in README
Returns whether or not the unit test passed, and if it didn't pass, gives a descriptive output to help debug.
*****NOTE***** These unit tests work only with the original input file, named origInput.txt */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "main.h"
#include "variableParser.h"
#include "simulator.h"
#include "states.h"
#include "time.h"

// Constants
#define Color_Red "\x1b[31m"
#define Color_Green "\x1b[32m"
#define Color_end "\x1b[0m"


// Global Variables
int totalTests = 0;
int failedTests = 0;
int passedTests = 0;


/* Given two stops, and a path, checks whether the shortest path between these
two stops is equal to the path specified.
If unit test fails, end output shows which parameters made the unit test fail.*/
void test_reconstructPath(int a, int b, int path[]) {
    int test_reconstructPathBool = 0;

    totalTests++;
    int *p = reconstructPath(a, b);
    int i;
    for (i = 0; i < 3; i++) {
        if (path[i] != p[i]) { // If there is some index where the values are not equal, set boolean to 0
            test_reconstructPathBool = 0;
            break; // We have found one index where values aren't equal, so we can break and fail the unit test
        } else {
            test_reconstructPathBool = 1; //
        }
    }
    free(p);
    sizeOfPath = 0;

        if (test_reconstructPathBool == 0) {
            failedTests++;
            printf("test_reconstructPath FAILED with parameters stop a = %d and stop b = %d.\n", a, b);
        } else {
            passedTests++;
        }
}


/* Given two stops, and a distance, checks whether the minimum distance between two
stops is equal to the distance specified.
If unit test fails, end output shows which parameters made the unit test fail. */
void test_distMatrix(int a, int b, int distance) {
    totalTests++;
    if (dist[a][b] == distance) {
        passedTests++;
    } else {
        failedTests++;
        printf("test_distMatrix FAILED with parameters stop b = %d and stop b = %d.\n", a, b);
    }
}


/* Given a string of form days:hours:minutes:seconds, and an integer, checks whether the integer (seconds)
is equal to specified distance.
Indicates PASSED if so, and FAILED otherwise.*/
void test_formatSecondsToString(char* s, int seconds) {
    totalTests++;
    char* formattedTime = formatSecondsToString(seconds);
    if (strcmp(formattedTime, s) == 0) {
        passedTests++;
    } else {
        failedTests++;
        printf("test_formatSecondsToString FAILED with parameters string = %s and seconds = %d.\n", s, seconds);
    }
    free(formattedTime);
}


/* Given a bus index, a destination stop, and a distance, checks whether the distance between
current position of bus and specified stop is indeed equal to specified distance.
Note here that the current position of any bus passed through to this function is always 0 (as bus starts off in garage).
Indicates PASSED if so, and FAILED otherwise.*/
void test_getDistanceFromBus(int bus, int stop, int distance) {
    totalTests++;
    if (getDistanceFromBus(bus, stop) == distance) {
        passedTests++;
    } else {
        failedTests++;
        printf("test_getDistanceFromBus FAILED with parameters bus = %d and stop = %d.\n", bus, stop);
    }
}


/* Given a bus index and a stop number, checks whether the closest bus to the given stop
is equal to the specificed bus passed through to this function.
Indicated PASSED if so, and FAILED otherwise */
void test_getClosestBus(int stop, int bus) {
    totalTests++;
    if (getClosestBus(stop) == bus) {
        passedTests++;
    } else {
        failedTests++;
        printf("test_getClosestBus FAILED with parameters bus = %d and stop = %d.\n", bus, stop);
    }
}


/* Calls departureStop() 15 times and tests whether all outputs of the function are between
0 and noStops-1 as this is the range of stops.*/
void test_departureStop() {
    int i = 0;
    while (i < 15) {
        totalTests++;
        int stop = getDepartureStop();
        if (stop < 0 || stop >= noStops) {
            printf("test_departureStop failed on instance %d. %d was generated as a stop. All stops generated should be between 0 and (noStops-1).\n", i, stop);
            failedTests++;
        }
        i++;
        passedTests++;
    }
}


/* Given a departure stop, generates 15 destination stops and tests whether all generated
destination stops are not equal to the departure stop, and all destination stops are
between 0 and noStops -1.*/
void test_destinationStop(int departureStop) {
    int i = 0;
    while (i < 10) {
        totalTests++;
        int stop = getDestinationStop(departureStop);
        if (stop < 0 || stop >= noStops || stop == departureStop) {
            printf("test_destinationStop failed on instance %d with parameter %d. %d was generated as a stop. All stops generated should be between 0 and (noStops-1), and unequal to departure stop.\n",
                i, departureStop, stop);
            failedTests++;
        }
        i++;
        passedTests++;
    }
}


/* Given a bus index and a departure time, returns the index of buses[busIndex].scheduledPickupTime[j]
which contains the pickup time  that would occur immediately before the departure time. Checks whether
the index returned by the function getClosestPickupBefore() is equal to the value of index passed in to
this function. */
void test_getClosestPickupBefore(int busIndex, int departureTime, int index) {
    totalTests++;
    if (getClosestPickupBefore(busIndex, departureTime) == index) {
        passedTests++;
    } else {
        failedTests++;
        printf("test_getClosestPickupBefore FAILED with parameters bus = %d and departureTime = %d.\n", busIndex, departureTime);
    }
}


/* Given a bus index and a departure time, returns the index of buses[busIndex].scheduledPickupTime[j]
which contains the pickup time  that would occur immediately after the departure time. Checks whether
the index returned by the function getClosestPickupAfter() is equal to the value of index passed in to
this function.*/
void test_getClosestPickupAfter(int busIndex, int departureTime, int index) {
    totalTests++;
    if (getClosestPickupAfter(busIndex, departureTime) == index) {
        passedTests++;
    } else {
        failedTests++;
        printf("test_getClosestPickupAfter FAILED with parameters bus = %d and departureTime = %d.\n", busIndex, departureTime);
    }
}



/* This function runs all unit tests declared below with a mixture of parameters for each unit test*/
void runUnitTests() {
    printf("***************************************************************************\n");
    printf("                            Unit Test Summary                    \n");
    printf("***************************************************************************\n\n");

    int arr[] = {0,1,2};
    int arr1[] = {0,5,4};
    int arr2[] = {4,1,2};
    int arr3[] = {2,3,4,1,0};
    test_reconstructPath(0, 2, arr);
    test_reconstructPath(0,4, arr1);
    test_reconstructPath(4,2, arr2);
    test_reconstructPath(2,0, arr3);

    test_distMatrix(0,1,3);
    test_distMatrix(0,2,8);
    test_distMatrix(5,0,4);
    test_distMatrix(1,2,5);
    test_distMatrix(3,2,8);
    test_distMatrix(0,5,4);

    test_formatSecondsToString("01:00:00:00", 86400);
    test_formatSecondsToString("00:01:00:00", 3600);
    test_formatSecondsToString("00:00:01:00", 60);
    test_formatSecondsToString("00:00:00:59", 59);
    test_formatSecondsToString("00:01:26:54", 5214);

    test_getDistanceFromBus(1, 5, 4);
    test_getDistanceFromBus(2, 3, 6);
    test_getDistanceFromBus(2, 2, 8);
    test_getDistanceFromBus(2, 4, 8);
    test_getDistanceFromBus(2, 1, 3);

    buses[4].currentPosition = 5;
    buses[3].currentPosition = 2;
    buses[2].currentPosition = 4;
    buses[1].currentPosition = 3;
    test_getClosestBus(4, 2);
    test_getClosestBus(5, 4);
    test_getClosestBus(3, 1);
    test_getClosestBus(1, 2);
    test_getClosestBus(0, 0);
    test_getClosestBus(2, 3);

    test_departureStop(); // only need to run once because loop within unit test runs 10 times

    test_destinationStop(0); // each function call generates 10 destination stops
    test_destinationStop(1);
    test_destinationStop(2);
    test_destinationStop(3);
    test_destinationStop(4);
    test_destinationStop(5);

    buses[0].scheduledPickupTime[0] = 120;
    buses[0].scheduledPickupTime[1] = 4931;
    buses[0].scheduledPickupTime[2] = 2692;
    buses[0].scheduledPickupTime[3] = 12119;
    buses[0].scheduledPickupTime[4] = 8000;
    test_getClosestPickupAfter(0, 6000, 4);
    test_getClosestPickupAfter(0, 0, 0);
    test_getClosestPickupAfter(0, 2691, 2);
    test_getClosestPickupAfter(0, 10000, 3);
    test_getClosestPickupAfter(0, 4500, 1);
    buses[0].scheduledPickupTime[0] = -1;
    buses[0].scheduledPickupTime[1] = -1;
    buses[0].scheduledPickupTime[2] = -1;
    buses[0].scheduledPickupTime[3] = -1;
    buses[0].scheduledPickupTime[4] = -1;
    test_getClosestPickupAfter(0, 4500, -1);


    buses[0].scheduledPickupTime[0] = 120;
    buses[0].scheduledPickupTime[1] = 4931;
    buses[0].scheduledPickupTime[2] = 2692;
    buses[0].scheduledPickupTime[3] = 12119;
    buses[0].scheduledPickupTime[4] = 8000;
    test_getClosestPickupBefore(0, 6000, 1);
    test_getClosestPickupBefore(0, 0, -1);
    test_getClosestPickupBefore(0, 2691, 0);
    test_getClosestPickupBefore(0, 10000, 4);
    test_getClosestPickupBefore(0, 4500, 2);



    printf("\n\nTotal Tests:        %d\n", totalTests);
    printf("Passed:             %d\n", passedTests);
    printf("Failed:             %d\n\n", failedTests);

    if (failedTests == 0) {
        printf("\033[1;32mAll unit tests have passed.\033[0m\n\n");
    } else {
        printf("\033[1;31m%d unit test(s) failed. The summary above indicates which unit tests did not pass.\033[0m\n\n", failedTests);
    }

}