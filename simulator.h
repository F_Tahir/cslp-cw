// Includes all function prototypes and global variables in the simulator.c source file


// Global variables with descriptions
#define NO_PATH 20000000 // constant used to set distances of vertices we cannot travel to
extern int **dist; // stores length of shortest path between two bus stops
extern int **nextHop; // the next hop of each vertex
extern int sizeOfPath; // start off with an array of size 1, and dynamically allocate space as path gets bigger
                            // needs to be global so other methods can access the size of path
extern int *path; // used in reconstructPath method - stores the actual (shortest) path from one stop to another
extern int userIndex;
extern int noUsers; // Everytime a new user comes in, this is incremented, whether accomodated or not
extern int totalRequests;
extern int missedRequests;
extern int closestEventBusID;
extern int closestEventUserID;
extern int closestEventBusStop;
extern int indexOfBusArray; // Stores the index of the bus array where the current users information is held
extern int closestEventID;


// Function uses are described in simulator.c
void calculateMinimumDist();
int *reconstructPath(int a, int b);
double uniform_deviate(int seed);
int getEventTime(double rate);
int getDepartureStop();
int getDestinationStop(int departureStop);
int getClosestBus(int departureStop);
int getDistanceFromBus(int closestBus, int departureStop);
int getMinIndexFromArr(int *arr, int sizeOfArr);
void setCurrentPathOfBus(int busIndex, int departureStop, int destinationStop);
int getClosestPickupBefore(int busIndex, int departureTime);
int getClosestPickupAfter(int busIndex, int departureTime);
char* formatSecondsToString(int seconds);
int assignScheduleTime(int departureTime, int departureStop, int destinationStop);

void startSimulator();
