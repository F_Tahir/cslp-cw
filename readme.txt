Project:                            Bus Simulation Software
Author:                            Faizan Tahir
Development Platform:    Ubuntu 15.04/DiCE (same compiler and text editor used throughout)

INSTRUCTIONS ON HOW TO RUN ARE GIVEN BELOW






What is it?
---------------------------------------------------------------
This bus simulation software, as part of my "Computer Science Large Practical 2015 - 2016" submission is a software
programmed in C. It allows the user to simulate how a bus company would operate by using an on-demand request system.
The user can specify parameters such as bus capacity, number of buses in service, request rates to simulate various scenarios.
A simulator will enable companies to imitate the real word operation of a bus system over a set time. The simulator outputs
various statistics that allow the user to measure how well the simulators heuristic methods are, as well as what the optimal
parameters for the system will be.








Running Instructions
---------------------------------------------------------------
1) Ensure all 6 source and header files listed above are in the same directory as the make file.
2) Open a terminal, ensuring you are in the same directory as where the makefile (and .c/.h files) are contained.
3) Type "make" into the terminal. This will create an executable file called "simulator".
4) To execute this file, type "./simulator <insert text file here>" into the terminal.
    4.1) Because all of my input scripts are in a folder called "Input Scripts", this will have to be declared in the path. E.g:
            ./simulator "Input Scripts/origInput.txt"
            should successfully run the script titled origInput.txt, assuming "make" was successfully typed in the terminal.
5) If the text file given by the user is valid, the simulator will run, otherwise an error will be thrown and program will exit.
    5.1) If an error occurs, ensure the file path is correct.







Optional parameters when executing
---------------------------------------------------------------
An optional parameter can be used when executing if the user wishes to run unit tests. Using this parameter,
the simulator will not run, instead, the functions in unitTests.c will be called, and the output will state
whether or not all unit tests have passed.

                        +-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
                         |                                               The unit tests can be run by typing ./simulator "Input Scripts/origInput.txt" -runtests                                 |
                        +-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Remark: It is important that the input script given is precisely "Input Scripts/origInput.txt" as above.
              This is because unit tests were carried out using the variables given in that input script,
              and which ever functions are being tested, the outputs may differ depending on what the
              parameters in the script are.










Contents
---------------------------------------------------------------
The final submitted version contains 6 .c source files, and for each source file, a corresponding .h header file. These files are:

main.c/main.h                                   Used to initially run the simulator, as well as link all functions from various source files together.

variableParser.c/variableParser.h       Contains functions that parse the input script and stores the parameters in global variables.
                                                        Input script is checked for errors and simulator won't run if any errors are present.

simulator.c/simulator.h                      Houses the main functions used in the simulator, such as heuristic methods and path planning.

states.c/states.h                               Includes structures and methods for altering structure values to store the state of the simulator.

output.c/output.h                              Includes functions that print out events as well as calculate and output statistics.

unitTests.c/unitTests.h                      Includes functions to test various functions of the simulator. Indicates which tests fail (if any).
                                                        Important remark regarding unit testing in "Optional parameters when executing" section







Input Scripts and makefile
---------------------------------------------------------------
I have included a folder called "Input Scripts" containing 10 invalid input scripts, each commented with their errors, and 7 valid
input scripts. Instructions on running the file with the given input scripts can be found below. As recommended in the handout,
I have also stored the outputs for some of the valid input scripts. These are titled "validScriptx_output.txt", where x the number
of the script.

I have also included a makefile in the main directory, again with instructions on how to run above.






