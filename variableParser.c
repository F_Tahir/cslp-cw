/* This file houses all functions needed for parsing the input file. Appropriate error handling
is carried out and errors, where required, are outputted to the user*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include "main.h"
#include "simulator.h"

// Global Variables
FILE *inputFile;


/* Function credit goes to RosettaCode.com - checks whether a stream of
characters are numeric (integer or float). Returns 1 if so, and 0 otherwise. */
int isNumeric (const char * s)
{
    if (s == NULL || *s == '\0' || isspace(*s))
      return 0;
    char * p;
    strtod (s, &p);
    return *p == '\0';
}


/* Parses input file line by line, storing contents of each line in an array, ignoring lines where
the first token is "#" (i.e. a comment), and extracting data from all other lines that hold
variable values. Error checking is also done here (i.e. ensuring values are float/unsigned int
per the specifications - useful warnings given if input file is incorrect and cannot be parsed

freeMemory() is called at every exit point to ensure any dynamically created arrays are no
longer being kept in memory even after program terminates*/
void extractVariables(char *fileName) {
    inputFile = fopen(fileName, "r");
    fileOpen = 1;
    char ch[2000]; // assumption that each line has no more than 2000 characters
    char *token[65540]; // assumption that each line has no more than 65540 words (max number of stops is 63535)
    int tokenCount = 0;

    // Read the input file line by line
    while ( fgets(ch, sizeof ch, inputFile ) != NULL ) {
        int x;

        if(ch[0] == '\n' || ch[0] == '#') { // skip blank lines and comments that have no whitespace (i.e. "#comment")
            continue;
        }

        token[0] = strtok(ch, " \n\t" );
        if ( strcmp( token[0], "#" ) == 0 ) {  // skip comments (different to line above - this skips comments with whitespace)
            continue;
       }

        // break the line into tokens - we can then proceed and check what variable each line represents
       // by looking at the first token
        int i;
        for ( i = 1; i < 65540; i++ ) {
            if ( (token[i] = strtok( NULL, " \n\t" )) == NULL ) {
                break;
            }
        }
        tokenCount = i;


        if (strcmp(token[0], "busCapacity") == 0) {
            // Check token count to ensure only one value is declared in input file
            if (tokenCount == 2) {
                // Second condition ensures only integer values are given (no floats allowed)
                if (isNumeric(token[1]) && floor(strtod(token[1], NULL)) == strtod(token[1], NULL) && strtol(token[1], NULL, 10) > 0) {
                    busCapacity = strtol(token[1], NULL, 10);
                } else {
                    printf("Error: Incorrect value for \"busCapacity\" variable. \"%s\" should be numeric, integer and greater "
                        "than 0. Please fix input file and re-run the simulator.\n", token[1]);
                    freeMemory(); // Ensure we clear noBusesArr and maxDelayArr if memory has been allocated
                    exit(1);
                }
            } else {
                printf("Error: Incorrect number of values for \"busCapacity\" variable. There should be exactly one "
                    "value. Please fix input file and re-run the simulator.\n");
                freeMemory(); // As above
                exit(1);
            }
        }

        // Works similar to busCapacity
        else if (strcmp(token[0], "boardingTime") == 0) {
            if (tokenCount == 2) {
                if (isNumeric(token[1]) && floor(strtod(token[1], NULL)) == strtod(token[1], NULL) && strtol(token[1], NULL, 10) >= 0) {
                    boardingTime = strtol(token[1], NULL, 10);
                } else {
                    printf("Error: Incorrect value for \"boardingTime\" variable. \"%s\" should be numeric, integer and greater "
                        "than 0. Please fix input file and re-run the simulator.\n", token[1]);
                    freeMemory();
                    exit(1);
                }
            } else {
                printf("Error: Incorrect number of values for \"boardingTime\" variable. There should be exactly one value. "
                    "Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
            }
        }

         // Works similar to busCapacity, with only difference being that floats are allowed
        else if (strcmp(token[0], "requestRate") == 0) {
            if (tokenCount == 2) {
                if (isNumeric(token[1]) && strtod(token[1], NULL) > 0.0) {
                    requestRate = strtod(token[1], NULL);
                } else {
                    printf("Error: Incorrect value for \"requestRate\" variable. \"%s\" should be numeric and greater than "
                        "0. Please fix input file and re-run the simulator.\n", token[1]);
                    freeMemory();
                    exit(1);
                }
            } else {
                printf("Error: Incorrect number of values for \"requestRate\" variable. There should be exactly one value. "
                    "Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
            }
        }

         // Works similar to busCapacity, with only difference being that floats are allowed
        else if (strcmp(token[0], "pickupInterval") == 0) {
            if (tokenCount == 2) {
                if (isNumeric(token[1]) && strtod(token[1], NULL) > 0.0) {
                    pickupInterval = strtod(token[1], NULL);
                } else {
                    printf("Error: Incorrect value for \"pickupInterval\" variable. \"%s\" should be numeric and greater than "
                        "0. Please fix input file and re-run the simulator.\n", token[1]);
                    freeMemory();
                    exit(1);
                }
            } else {
                printf("Error: Incorrect number of values for \"pickupInterval\" variable. There should be exactly one "
                    "value. Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
            }
        }


        else if (strcmp(token[0], "maxDelay") == 0) {
             if (tokenCount > 2) {
                if (strcmp(token[1], "experiment") == 0) { // Check if experiment keyword is declared
                    maxDelayExperBool = 1;
                    maxDelay = 0;
                    sizeOfMaxDelayArr = tokenCount - 2; // Discard the tokens "maxDelay" and "experiment"
                    // Dynamically allocate array because we do not know what the size should be before runtime.
                    // This is only known after parsing and checking tokenCount
                    maxDelayArr = malloc(tokenCount * sizeof(int));
                    for (x = 2; x < tokenCount ; x++) {
                        if (isNumeric(token[x]) && floor(strtod(token[x], NULL)) == strtod(token[x], NULL) && strtol(token[x], NULL, 10) >= 0) {
                            maxDelayArr[x-2] = strtol(token[x], NULL, 10);
                        } else { // will trigger on a value such as "maxDelay experiment 3 x"
                            printf("Error: Incorrect value for \"maxDelay\" variable. The token \"%s\" should be numeric, integer and greater than "
                                "0. Please fix input file and re-run the simulator.\n", token[x]);
                            freeMemory();
                            exit(1);
                        }
                    }
                    if (sizeOfMaxDelayArr == 1) { // e.g. "maxDelay experiment 1" - experiment keyword is redundant as only one value declared
                        printf("Error: Experiment keyword declared in input file for \"maxDelay\" variable but only one value is declared. "
                            "Please declare more values or omit the experiment keyword.\n");
                        freeMemory();
                        exit(1);
                    }
                } else { // number of tokens greater than 2 but "experiment" keyword not present i.e "maxDelay 2 5"
                    printf("Error: Incorrect number of values for \"maxDelay\" variable. There should be exactly one value, or more if "
                        "the \"experiment\" keyword is declared in the second token. Please fix input file and re-run the simulator.\n");
                    freeMemory();
                    exit(1);
                }
             } else if (tokenCount == 2) { //  i.e. "maxDelay x"
                if (isNumeric(token[1]) && floor(strtod(token[1], NULL)) == strtod(token[1], NULL) && strtol(token[1], NULL, 10) >= 0) {
                    maxDelay = strtol(token[1], NULL, 10);
                } else {  // second token is non numeric (i.e maxDelay x)
                    printf("Error: Incorrect value for \"maxDelay\" variable. The token \"%s\" should be numeric, integer and greater "
                        "than 0. Ensure the word \"experiment\" is not declared without any values. Please fix input file and re-run the simulator.\n", token[1]);
                    freeMemory();
                    exit(1);
                }
             } else if (tokenCount < 2) { // i.e. "maxDelay" (no value assigned)
                printf("Error: Incorrect number of values for \"maxDelay\" variable. There should be exactly one value, or more if "
                    "the \"experiment\" keyword is declared. Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
             }

         }


         // Works same as the above maxDelay code
        else if (strcmp(token[0], "noBuses") == 0) {
             if (tokenCount > 2) {
                if (strcmp(token[1], "experiment") == 0) {
                    noBusesExperBool = 1;
                    noBuses = 0;
                    sizeOfNoBusesArr = tokenCount - 2;
                    noBusesArr = malloc(tokenCount * sizeof(int));
                    for (x = 2; x < tokenCount ; x++) {
                        if (isNumeric(token[x]) &&  floor(strtod(token[x], NULL)) == strtod(token[x], NULL) && strtol(token[x], NULL, 10) > 0) {
                            noBusesArr[x-2] = strtol(token[x], NULL, 10);
                        } else {
                            printf("Error: Incorrect value for \"noBuses\" variable. The token \"%s\" should be numeric, integer and greater "
                                "than 0. Please fix input file and re-run the simulator.\n", token[x]);
                            freeMemory();
                            exit(1);
                        }
                    }
                    if (sizeOfNoBusesArr == 1) {
                        printf("Error: Experiment keyword declared in input file for \"noBuses\" variable but only one value is declared. "
                            "Please declare more values or omit the experiment keyword.\n");
                        freeMemory();
                        exit(1);
                    }
                } else {
                    printf("Error: Incorrect number of values for \"noBuses\" variable. There should be exactly one value, "
                        "or more if the \"experiment\" keyword is declared. Please fix input file and re-run the simulator.\n");
                    freeMemory();
                    exit(1);
                }
             } else if (tokenCount == 2) {
                if (isNumeric(token[1]) && floor(strtod(token[1], NULL)) == strtod(token[1], NULL) && strtol(token[1], NULL, 10) > 0) {
                    noBuses = strtol(token[1], NULL, 10);
                } else {
                    printf("Error: Incorrect value for \"noBuses\" variable. The token \"%s\" should be numeric, integer and greater "
                        "than 0. Ensure the word \"experiment\" is not declared without any values. Please fix input file and "
                        "re-run the simulator.\n", token[1]);
                    freeMemory();
                    exit(1);
                }
             } else if (tokenCount < 2) {
                printf("Error: Incorrect number of values for \"noBuses\" variable. There should be exactly one value, or more "
                    "if the \"experiment\" keyword is declared in the second token. Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
             }

         }


        // Works similar to busCapacity
        else if (strcmp(token[0], "noStops") == 0) {
            if (tokenCount == 2) {
                if (isNumeric(token[1]) && floor(strtod(token[1], NULL)) == strtod(token[1], NULL) && strtol(token[1], NULL, 10) > 0) {
                    noStops = strtol(token[1], NULL,10);
                } else {
                    printf("Error: Incorrect value for \"noStops\" variable. \"%s\" should be numeric, integer and greater than 0. "
                        "Please fix input file and re-run the simulator.\n", token[1]);
                    freeMemory();
                    exit(1);
                }
            } else {
                printf("Error: Incorrect number of values for \"noStops\" variable. There should be exactly one value. "
                    "Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
            }
        }


         // Works similar to busCapacity
        else if (strcmp(token[0], "stopTime") == 0) {
            if (tokenCount == 2) {
                if (isNumeric(token[1]) && strtod(token[1], NULL) > 0.0) {
                    stopTime = strtod(token[1], NULL);
                } else {
                    printf("Error: Incorrect value for \"stopTime\" variable. \"%s\" should be numeric and greater than 0. "
                        "Please fix input file and re-run the simulator.\n", token[1]);
                    freeMemory();
                    exit(1);
                }
            } else {
                printf("Error: Incorrect number of values for \"stopTime\" variable. There should be exactly one value. "
                    "Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
            }
        }


        // "map" keyword is declared on its own line, so tokenCount has to equal 1 otherwise input file is syntactically wrong
        else if (strcmp(token[0], "map") == 0 && tokenCount == 1) {
            if (noStops <= 0) {
                printf("Error: The input file has either declared noStops as negative (or 0), or no value is present,"
                    " so \"map\" cannot be parsed. ");
                printf("This could be because \"noStops\" was declared after \"map\" in the input file, in which "
                    "case \"noStops\" should be declared FIRST. ");
                printf("Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
            } else {
                mapBoolean = 1;
                // Dynamically allocate array size as we do not know noStops value before runtime
                // Matrix is of dimension noStops * noStops
                mapMatrix = malloc(sizeof(int*)*noStops);
                for (x = 0; x < noStops; x++)
                    mapMatrix[x] = malloc(sizeof(int)*noStops);
                continue;
                }
            }


        // Second condition ensures that the matrix values come AFTER map keyword in input file
        else if(isNumeric(token[0]) && mapBoolean == 1) {
            if (tokenCount == noStops) {
                for (x = 0; x < tokenCount; x++) {
                    // Want to ensure that matrix values are integer only (no floats allowed)
                    if (isNumeric(token[x]) && floor(strtod(token[x], NULL)) == strtod(token[x], NULL)) {
                        if (noMatrixRows >= noStops) { // I.e. matrix is not n * n (as it should be)
                            printf("Error: Too many rows in the matrix. The number of rows and columns should both equal \"noStops\" (%d). "
                                " Please fix the input file and re-run the simulator.", noStops);
                            freeMemory();
                            exit(1);
                        } else {
                            mapMatrix[noMatrixRows][x] = strtol(token[x], NULL, 10); // Convert the token to an integer type
                        }
                    } else {
                        printf("Error: Incorrect value for \"map\" matrix. \"%s\" should be numeric and integer. "
                            "Please fix this and re-run the simulator.\n", token[x]);
                        freeMemory();
                        exit(1);
                    }
                }
                noMatrixRows++;
            } else {
                printf("Error: Invalid input file. The map matrix has incorrect number of values in one or more of its rows."
                    " The number of values in each row should be equal to the value \"noStops\" (%d). ", noStops);
                printf("Please fix input file and re-run the simulator.\n");
                freeMemory();
                exit(1);
            }
        }


        else {
            printf("Error: Invalid input file. This could be because: \n");
            printf("1) one or more of the non-comment lines do not start with a variable keyword;\n");
            printf("2) a line in the input file starts with an integer but there is no \"map\" keyword directly above the line.\n");
            printf("Please fix input file and re-run the simulator.\n");
            freeMemory();
            exit(1);
        }
    }

     if (fileOpen == 1) { // Used for freeMemory() function
            fclose(inputFile);
            fileOpen = 0;
        }
}



/* This function checks that the values have actually been declared in the input file. Values are equal to 0 by default,
and if they are still equal to 0 after parsing then this suggests that the values were not declared in the input file */
void checkVariables() {
    int i, j;

    // Default value is 0, if value is still 0 after parsing, this suggests no value was declared in the input file; throw error and exit
    if (busCapacity <= 0) {
        printf("Error: busCapacity has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    /* boardingTime is originally initiated to 20000000. If this is still the same value after parsing is completed,
     this suggests no value was declared in the input file. I do this because in my personal opinion, an input file
     with a value 0 for boardingTime (and maxDelay) should be valid, but because they are unsigned ints, I could
     think of no other way to ensure a value was declared in the input file. Declaring as -1 would not work because
     this would be converted to a large number due to being unsigned */
    } else if (boardingTime == 20000000) {
        printf("Error: boardingTime has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    } else if (requestRate <= 0.0) {
        printf("Error: requestRate has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    } else if (pickupInterval <= 0.0) {
        printf("Error: pickupInterval has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    } else if (maxDelay == 20000000 && maxDelayExperBool == 0) {
        printf("Error: maxDelay has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    } else if (noBuses <= 0 && noBusesExperBool == 0) {
        printf("Error: noBuses has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    } else if (noStops <= 0) {
        printf("Error: noStops has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    } else if (stopTime <= 0) {
        printf("Error: stopTime has no value in input file. Please fix input file and re-run the simulator.\n");
        freeMemory();
        exit(1);

    } else if (mapBoolean <= 0) {
        printf("Error: Matrix of map not parsed. This could be because \"map\" variable is not present in input file, "
            "or because \"noStops\" value is not present. Please fix input file and re-run the simulator.");
        freeMemory();
        exit(1);

     } else if (noMatrixRows != noStops) {
        printf("Error: Map matrix should have %d rows in the input file. Please fix input file and "
            "re-run the simulator.\n", noStops);
        freeMemory();
        exit(1);

    // Ensures diagonal values of matrix are 0, and all other values are -1 or greater than 0
    } else if (mapBoolean == 1) {
        for (i = 0; i < noStops; i++) {
            for (j = 0; j < noStops; j++) {
                if (i == j) {
                    if (mapMatrix[i][j] != 0) {
                        printf("Error: Diagonal values of map matrix must be 0. Row %d, Column %d (starting count "
                            "from 0) is the culprit of this error. Please fix input file and re-run the simulator.\n", i, j);
                        freeMemory();
                        exit(1);
                    }
                } else {
                    if (mapMatrix[i][j] < -1 || mapMatrix[i][j] == 0 ) {
                        printf("Error: Non-diagonal values of map matrix must be -1 or greater than 0. Row %d, "
                            "Column %d (starting count from 0) is the culprit of this error. Please fix input file and "
                            "re-run the simulator.\n", i, j);
                        freeMemory();
                        exit(1);
                    }
                }
            }
        }
    }
    variableParsingOk = 1;
    // boolean set to 1 if error checking returns no errors

}

